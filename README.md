![image alt ><](./icons/DOCKS_icon.png)
<h1 style="text-align: center;">DOCKS Propagator

# From version 4.3.0

Dear users, we will no longer maintain DOCKS in the main Gitlab.com servers.

DOCKS is still open-source. We have transfered the public releases of DOCKS to our own servers:
- Presentation of DOCKS: [clic here](https://census.psl.eu/spip.php?rubrique29&lang=en)
- All DOCKS modules (for dev.): [clic here](https://gitlab.obspm.fr/public_docks/)
- Releases:
	-	[DOCKS Propagator](https://gitlab.obspm.fr/public_docks/trajectories/-/releases)
	-	[DOCKS Intervisibility](https://gitlab.obspm.fr/public_docks/intervisibilities/-/releases)

# Bugs and Suggestions

Report any bugs to [DOCKS GitLab](https://gitlab.obspm.fr/public_docks/trajectories) at CENSUS, Paris Observatory-PSL.

Provide suggestions to [DOCKS mail](mailto:docks.contact@obspm.fr).

Try our remote service by sending the Subject: `[server]` to [DOCKS mail](mailto:docks.contact@obspm.fr), you will be replied with a path to your own folder on our server and README files on how to proceed.

Thank you for your interest.

![image](icons/CENSUS_icon.png)
<h2 style="text-align: center;">DOCKS by CENSUS</h2>
<img src="./icons/Observatoire_de_Paris-CoMarquage-RGB_Ultra_violet.png" alt="drawing" width="400" style="float: right;"/>
